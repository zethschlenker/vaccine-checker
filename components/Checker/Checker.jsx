import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import 'bootstrap/dist/css/bootstrap.min.css';
import useSound from 'use-sound';
import styled from 'styled-components';
import queryLocations from './queryLocations';
import notificationSound from './sound/notificationSound.mp3';

const CheckerWrap = styled.div`
  text-align: center;
`;

export default function App() {
  const today = new Date();
  const [checked, setChecked] = useState(
    `${today.toLocaleDateString()} - ${today.toLocaleTimeString()}`,
  );
  const [locations, setLocations] = useState([]);

  const defaultPageTitle = 'Vaccine Checker';
  const [pageTitle, setPageTitle] = useState(defaultPageTitle);
  const [notify] = useSound(notificationSound, {
    volume: 1.00,
  });

  useEffect(() => {
    const handleInterval = () => {
      queryLocations(
        (date) => {
          setChecked(
            `${date.toLocaleDateString()} - ${date.toLocaleTimeString()}`,
          );
        },
        (results) => {
          setLocations(results);
          if (results.length) {
            notify();

            setPageTitle(`(${results.length}) ${defaultPageTitle}`);
          } else {
            setPageTitle(defaultPageTitle);
          }
        },
      );
    };

    handleInterval();
    const interval = setInterval(handleInterval, 10000);
    return () => clearInterval(interval);
  }, [notify]);

  return (
    <CheckerWrap>
      <Head>
        <title>{pageTitle}</title>
      </Head>
      <div className="container">
        <h1>Hy-Vee COVID Checker</h1>
        <h2>Last Checked: {checked}</h2>
        <h3>Locations Found: {locations.length}</h3>
        <p>(checking within 60 miles of Lincoln, NE)</p>
      </div>
      {locations.length > 0 && (
        <div className="container">
          <p>
            Visit the{' '}
            <a
              href="https://www.hy-vee.com/my-pharmacy/covid-vaccine-consent"
              target="_blank"
              rel="noreferrer"
            >
              Hy-Vee Website
            </a>{' '}
            and search the city from your results. Sorry it's not easier right
            now!
          </p>
          <table className="table table-striped table-bordered">
            <thead className="thead-dark">
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Nickname?</th>
                <th scope="col">Distance (From Lincoln)</th>
                <th scope="col">Eligibility Terms</th>
              </tr>
            </thead>
            <tbody>
              {locations.map((location) => (
                <tr key={location.location.locationId}>
                  <td>{location.location.name}</td>
                  <td>{location.location.nickname}</td>
                  <td>{location.distance} miles</td>
                  <td>{location.location.covidVaccineEligibilityTerms}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </CheckerWrap>
  );
}
