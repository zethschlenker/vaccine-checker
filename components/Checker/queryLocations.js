const queryLocations = (checkCallback, foundCallback) => {
  fetch('/api/hyvee-checker', {
    method: 'GET',
    mode: 'cors',
  })
    .then((r) => r.json())
    .then((data) => {
      const results = data.data.searchPharmaciesNearPoint;
      // const covidAvailable = results;
      const covidAvailable = results.filter(
        (result) => result.location.isCovidVaccineAvailable,
      );

      checkCallback(new Date());
      foundCallback(covidAvailable);
    });
};

export default queryLocations;
