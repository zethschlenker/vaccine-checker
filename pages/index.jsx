import Head from 'next/head';
import Checker from '@components/Checker/Checker';

export default function Home() {
  return (
    <div>
      <Head>
        <title>Vaccine Checker</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Checker />
      </main>
    </div>
  );
}
