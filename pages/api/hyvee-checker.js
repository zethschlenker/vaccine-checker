const fetch = require('node-fetch');

const queryLocations = (req, res) => {
  const query = `query SearchPharmaciesNearPointWithCovidVaccineAvailability($latitude: Float!, $longitude: Float!, $radius: Int! = 10) {
    searchPharmaciesNearPoint(latitude: $latitude, longitude: $longitude, radius: $radius) {
      distance
      location {
        locationId
        name
        nickname
        phoneNumber
        isCovidVaccineAvailable
        covidVaccineEligibilityTerms
      }
    }
  }`;
  const latitude = 40.813616;
  const longitude = -96.7025955;
  const radius = 60;

  return fetch('https://www.hy-vee.com/my-pharmacy/api/graphql', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: '*/*',
    },
    body: JSON.stringify({
      query,
      variables: {
        latitude,
        longitude,
        radius,
      },
    }),
  })
    .then((r) => r.clone().json())
    .then((data) => res.status(200).json(data));
};

export default queryLocations;
